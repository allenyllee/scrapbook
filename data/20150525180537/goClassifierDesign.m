% Performance evaluation

load DS.mat
prior=dsClassSize(DS);		% Use the class size as the class prior probability
[qcPrm, recogRate]=qcTrain(DS, prior);
fprintf('Recognition rate = %f%%\n', recogRate*100);
model=qcPrm;
fprintf('Saving classifier''s parameters...\n');
save model model
