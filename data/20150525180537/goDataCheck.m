% Check data consistency for speaker identification.

load speakerData.mat
sidPrm=sidPrmSet;

% ====== Check empty speaker folder in session01
sentenceNum=[speakerData1.sentenceNum];
index=find(sentenceNum==0);
speakerData1Empty=speakerData1(index);
title='Speakers with no recordings in session 1';
outputFile=sprintf('%s/%s.htm', sidPrm.outputDir, title);
if ~isempty(speakerData1Empty), structDispInHtml(speakerData1Empty, title, {'name'}, [], [], outputFile); end
speakerData1(index)=[];
% ====== Check empty speaker folder in session02
sentenceNum=[speakerData2.sentenceNum];
index=find(sentenceNum==0);
speakerData2Empty=speakerData2(index);
title='Speakers with no recordings in session 2';
outputFile=sprintf('%s/%s.htm', sidPrm.outputDir, title);
if ~isempty(speakerData2Empty), structDispInHtml(speakerData2Empty, title, {'name'}, [], [], outputFile); end
speakerData2(index)=[];
% ====== Speaker difference in both sessions
speaker1={speakerData1.name};
speaker2={speakerData2.name};
diffSet1=setdiff(speaker1, speaker2);
diffSet2=setdiff(speaker2, speaker1);
% === Speaker in session01 but not in session02
index1=[];
for i=1:length(diffSet1)
	index1=[index1, find(strcmp(diffSet1{i}, speaker1))];
end
title='Speakers only in session 1';
outputFile=sprintf('%s/%s.htm', sidPrm.outputDir, title);
if ~isempty(index1), structDispInHtml(speakerData1(index1), title, {'name'}, [], [], outputFile); end
speakerData1(index1)=[];
% === Speaker in session02 but not in session01
index2=[];
for i=1:length(diffSet2)
	index2=[index2, find(strcmp(diffSet2{i}, speaker2))];
end
title='Speakers only in session 2';
outputFile=sprintf('%s/%s.htm', sidPrm.outputDir, title);
if ~isempty(index2), structDispInHtml(speakerData2(index2), title, {'name'}, [], [], outputFile); end
speakerData2(index2)=[];
