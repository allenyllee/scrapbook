% Feature extraction

sidPrm=sidPrmSet;

% ====== Read session 1
speakerData1=speakerDataRead(sidPrm.waveDir01);
fprintf('Get wave info of %d persons from %s\n', length(speakerData1), sidPrm.waveDir01);
speakerData1=speakerDataAddFea(speakerData1, sidPrm);	% Add features to speakerData1
% ====== Read session 2
speakerData2=speakerDataRead(sidPrm.waveDir02);
fprintf('Get wave info of %d persons from %s\n', length(speakerData2), sidPrm.waveDir02);
speakerData2=speakerDataAddFea(speakerData2, sidPrm);	% Add features to speakerData2

fprintf('Save speakerData1 and speakerData2 to speakerData.mat\n');
save speakerData speakerData1 speakerData2