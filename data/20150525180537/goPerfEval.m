% Performance evaluation
load speakerData.mat
% ====== Speaker ID by DTW
for i=1:length(speakerData2)
	tInit=clock;
	name=speakerData2(i).name;
	fprintf('%d/%d: speaker=%s\n', i, length(speakerData2), name);
	for j=1:length(speakerData2(i).sentence)
	%	fprintf('\tsentence=%d ==> ', j);
	%	t0=clock;
		inputSentence=speakerData2(i).sentence(j);
		[speakerIndex, sentenceIndex, minDistance]=speakerId(inputSentence, speakerData1);
		computedName=speakerData1(speakerIndex).name;
	%	fprintf('computedName=%s, time=%.2f sec\n', computedName, etime(clock, t0));
		speakerData2(i).sentence(j).correct=strcmp(name, computedName);
		speakerData2(i).sentence(j).computedSpeakerIndex=speakerIndex;
		speakerData2(i).sentence(j).computedSentenceIndex=sentenceIndex;
		speakerData2(i).sentence(j).computedSentencePath=speakerData1(speakerIndex).sentence(sentenceIndex).path;
	end
	speakerData2(i).correct=[speakerData2(i).sentence.correct];
	speakerData2(i).rr=sum(speakerData2(i).correct)/length(speakerData2(i).correct);
	fprintf('\tRR for %s = %.2f%%, ave. time = %.2f sec\n', name, 100*speakerData2(i).rr, etime(clock, tInit)/length(speakerData2(i).sentence));
end
correct=[speakerData2.correct];
overallRr=sum(correct)/length(correct);
fprintf('Ovderall RR = %.2f%%\n', 100*overallRr);

fprintf('Save speakerData1 and speakerData2 to speakerData.mat\n');
save speakerData speakerData1 speakerData2