sidPrm=sidPrmSet;
load speakerData.mat
correct=[speakerData2.correct];
overallRr=sum(correct)/length(correct);

% ====== Display each person's performance
[junk, index]=sort([speakerData2.rr]);
sortedSpeakerData2=speakerData2(index);
outputFile=sprintf('%s/personRr_rr=%f%%.htm', sidPrm.outputDir, 100*overallRr);
structDispInHtml(sortedSpeakerData2, sprintf('Performance of all persons (Overall RR=%.2f%%)', 100*overallRr), {'name', 'rr'}, [], [], outputFile);
% ====== Display misclassified utterances
sentenceData=[sortedSpeakerData2.sentence];
sentenceDataMisclassified=sentenceData(~[sentenceData.correct]);
outputFile=sprintf('%s/sentenceMisclassified_rr=%f%%.htm', sidPrm.outputDir, 100*overallRr);
structDispInHtml(sentenceDataMisclassified, sprintf('Misclassified Sentences (Overall RR=%.2f%%)', 100*overallRr), {'path', 'computedSentencePath'}, [], [], outputFile);
