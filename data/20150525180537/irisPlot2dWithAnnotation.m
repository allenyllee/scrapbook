DS=prData('iris');
DS.input=DS.input(3:4, :);
for i=1:length(DS.output)
	DS.annotation{i}=sprintf('Data index=%d, class=%s', i, DS.outputName{DS.output(i)});
end
opt.showAnnotation=1;
opt.showLegend=1;
dsScatterPlot(DS, opt);