function sidPrm=sidPrmSet
% sidPrmSet: Set parameters for speaker identification 

% ====== Add required toolboxes to the search path
mltPath='/users/jang/matlab/toolbox/dcpr';
addpath(mltPath);

% ====== Wave directories
sidPrm.waveDir01=sprintf('%s\dataSet\speakerIdTextDependent\session01', mltRoot);
sidPrm.waveDir02=sprintf('%s\dataSet\speakerIdTextDependent\session02', mltRoot);

sidPrm.feaType='mfcc';		% 'mfcc', 'volume', 'pitch'
sidPrm.outputDir='output';		% Output directory
